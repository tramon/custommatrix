﻿using System;
using System.Text;

namespace Matrix {

    // Class designed in a way, that operations can be made only with matrixes of same size:
    // (equal amount of rows and columns)
    
    // * Used materials:
    // * True random: https://msdn.microsoft.com/ru-ru/library/ctssatww(v=vs.110).aspx
    // * Matrix multiplication: https://en.wikipedia.org/wiki/Matrix_multiplication

    class CustomMatrix {

        private int[,] innerMatrix;
        private int rows = 4;
        private int columns = 4;
        Random random;

        public int this[int rows, int columns] {
            get {
                return innerMatrix[rows, columns];
            }
            set {
                innerMatrix[rows, columns] = value;
            }
        }

        public CustomMatrix() {
            innerMatrix = new int[rows, columns];
        }

        public CustomMatrix (int rows, int columns) {
            innerMatrix = new int[rows , columns];
            this.rows = rows;
            this.columns = columns;
        }

        static void Main(string[] args) {
            CustomMatrix matrix = new CustomMatrix(4, 4);
            matrix.fillMatrixWithRandomInts();
            Console.WriteLine("1. First matrix:\n" + matrix);

            CustomMatrix matrix2 = new CustomMatrix(4, 4);
            matrix2.fillMatrixWithRandomInts();
            Console.WriteLine("2. Second matrix:\n" + matrix2);

            CustomMatrix matrix3 = matrix.add(matrix2);
            Console.WriteLine("3. Result of Addition:\n" + matrix3);

            CustomMatrix matrix4 = matrix.subtract(matrix2);
            Console.WriteLine("4. Result of Subtraction:\n" + matrix4);

            CustomMatrix matrix5 = new CustomMatrix(3, 3);
            matrix5.fillMatrixWithFixedNumber(2);
            Console.WriteLine("5. Fifth matrix:\n" + matrix5);

            CustomMatrix matrix6 = new CustomMatrix(3, 3);
            matrix6.fillMatrixWithFixedNumber(3);
            matrix6.setIntInMatrixByIndex(1, 1, 100);
            Console.WriteLine("6. Sixth matrix:\n" + matrix6);

            CustomMatrix matrix7 = matrix5.multiply(matrix6);
            Console.WriteLine("7. Result of Multiplication:\n" + matrix7);


            Console.ReadKey();
        }

        public void fillMatrixWithRandomInts() {
            CustomMatrix matrix = this;
            random = new Random((int)DateTime.Now.Ticks);
            int randomInt = 0;
            int rowSize = matrix.getRows();
            int columnSize = matrix.getColumns();

            for (int i = 0; i < rowSize; i++) {
                for (int j = 0; j < columnSize; j++) {
                    randomInt = random.Next(-100, 100);
                    innerMatrix[i,j] = randomInt;
                }
            }
        }

        public void fillMatrixWithFixedNumber(int intNumber) {
            CustomMatrix matrix = this;
            int rowSize = matrix.getRows();
            int columnSize = matrix.getColumns();

            for (int i = 0; i < rowSize; i++) {
                for (int j = 0; j < columnSize; j++) {
                    innerMatrix[i, j] = intNumber;
                }
            }
        }

        public void setIntInMatrixByIndex (int row, int column, int value) {
            innerMatrix[row, column] = value;
        }

        public CustomMatrix add(CustomMatrix rightMatrix) {
            CustomMatrix leftMatrix = this;
            int rowSize = rightMatrix.getRows();
            int columnSize = rightMatrix.getColumns();
            CustomMatrix result = new CustomMatrix(rowSize, columnSize);

            try {
                if (!leftMatrix.Equals(rightMatrix)) {
                    throw new ArithmeticException();
                } else {
                    for (int i = 0; i < rowSize; i++) {
                        for (int j = 0; j < columnSize; j++) {
                            result[i, j] = leftMatrix[i, j] + rightMatrix[i, j];
                        }
                    }
                }
            } catch (ArithmeticException ex) {
                Console.Error.WriteLine("\nException: Please add matrixes of the same size!\n");
                Console.WriteLine(ex.ToString());
            }
            return result;
        }

        public CustomMatrix subtract(CustomMatrix rightMatrix) {
            CustomMatrix leftMatrix = this;
            int rowSize = rightMatrix.getRows();
            int columnSize = rightMatrix.getColumns();
            CustomMatrix result = new CustomMatrix(rowSize, columnSize);

            try {
                if (!leftMatrix.Equals(rightMatrix)) {
                    throw new ArithmeticException();
                } else {
                    for (int i = 0; i < rowSize; i++) {
                        for (int j = 0; j < columnSize; j++) {
                            result[i, j] = leftMatrix[i, j] - rightMatrix[i, j];
                        }
                    }
                }
            } catch (ArithmeticException ex) {
                Console.Error.WriteLine("\nException: Please subtract matrixes of the same size!\n");
                Console.WriteLine(ex.ToString());
            } 
            return result;
        }
         
        public CustomMatrix multiply(CustomMatrix rightMatrix) {
            CustomMatrix leftMatrix = this;
            int rowSize = rightMatrix.getRows();
            int columnSize = rightMatrix.getColumns();
            CustomMatrix result = new CustomMatrix(rowSize, columnSize);

            try {
                if (!leftMatrix.Equals(rightMatrix)) {
                    throw new ArithmeticException();
                } else {
                    for (int i = 0; i < rowSize; i++) {
                        for (int j = 0; j < columnSize; j++) {
                            for (int n = 0; n < rowSize; n++) {
                                result[i, j] += leftMatrix[i, n] * rightMatrix[n, j];
                            }
                        }
                    }
                }
            } catch (ArithmeticException ex) {
                Console.Error.WriteLine("\nException: Please multiply matrixes of the same size!\n");
                Console.WriteLine(ex.ToString());
            }
            return result;
        }

        public override string ToString() {
            StringBuilder stringBuilder = new StringBuilder();
            int rowSize = innerMatrix.GetLength(0);
            int columnSize = innerMatrix.GetLength(1);

            for (int i = 0; i < rowSize; i++) {
                for (int j = 0; j < columnSize; j++) {
                    stringBuilder.Append(innerMatrix[i, j] + "\t");
                }
                stringBuilder.Append("\n");
            }
            return stringBuilder.ToString();
        }

        public bool Equals(CustomMatrix rightMatrix) {
            CustomMatrix leftMatrix = this;

            if (leftMatrix.getRows() == rightMatrix.getRows() &&
                leftMatrix.getColumns() == rightMatrix.getColumns()
                ) {
                return true;
            }
            return false;
        }

        private int getRows() {
            return rows;
        }

        private int getColumns() { 
            return columns;
        }

    }
}
